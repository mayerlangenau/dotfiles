set -g theme_display_git yes
set -g theme_display_date no
set -g theme_display_user yes
set -g theme_title_display_hostname yes
set -g theme_title_display_user yes
set -g theme_color_scheme base16-light

alias ls exa

