#!/bin/bash

# Mousepad settings
gsettings set org.xfce.mousepad.preferences.window client-side-decorations false

# Caja settings
gsettings set org.mate.caja.preferences always-use-browser false
gsettings set org.mate.applications-terminal exec alacritty
