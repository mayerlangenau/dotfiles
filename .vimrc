python3 from powerline.vim import setup as powerline_setup
python3 powerline_setup()
python3 del powerline_setup

set laststatus=2

syntax on

set tabstop=4
set shiftwidth=4
set wrap

set number

set title
set visualbell
set noerrorbells
set showcmd
set showmode
"set statusline=%F%m%r%h%w\ [FORMAT=%{&ff}]\ [TYPE=%Y]\ [POS=%l,%v][%p%%]\ [BUFFER=%n]\ %{strftime('%c')}
set t_Co=256

"colorscheme nord

