# If not running interactively, don't do anything
[ -z "$PS1" ] && return
# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

alias ll='ls -l'

if [ -f `which powerline-daemon` ]; then
    if ps $PPID | grep -q "xterm\|tmux\|alacritty"; then
        POWERLINE_BASH_CONTINUATION=1
        POWERLINE_BASH_SELECT=1
        . /usr/share/powerline/bindings/bash/powerline.sh
    fi
fi
