#--------------------------------------------------------------------------
# Slack15 Style
#--------------------------------------------------------------------------

Emulate Fvwm

InfoStoreAdd font.small          "xft:Droid Sans:size=8::minspace=False:antialias=True"
InfoStoreAdd font.small.shadow   "Shadow=1 0 SE:xft:Droid Sans:size=8::minspace=False:antialias=True"
InfoStoreAdd font.normal         "xft:Droid Sans:size=9::minspace=False:antialias=True"
InfoStoreAdd font.normal.shadow	 "Shadow=1 0 SE:xft:Droid Sans:size=9::minspace=False:antialias=True"
InfoStoreAdd font.title          "xft:Droid Sans::size=10:minspace=False:antialias=True"
InfoStoreAdd font.title.shadow   "Shadow=1 0 SE:xft:Droid Sans:size=10::minspace=False:antialias=True"

read $[FVWM_USERDIR]/conf/colors/tomorrow_light

Style * Colorset 3, HilightColorset 4, BorderColorset 3, HilightBorderColorset 4
Style * BorderWidth 4, HandleWidth 4

Style * TitleFormat %i

Style fvwm_menu !Button 2, !Button 4, !Button 6, !Handles
Style xclock BorderWidth 3, HandleWidth 3
Style xload UseDecor PanelDecor, !Title, WindowListSkip, CirculateSkip, Sticky, NeverFocus, FixedSize, !Handles, BorderWidth 4, HandleWidth 4, \
Colorset 8, HighlightColorset 0, BorderColorset 5


DefaultColorset 0

# Fonts
#--------------------------------------------------------------------------
DefaultFont "$[infostore.font.normal.shadow]"

Style * Font      "$[infostore.font.title.shadow]"
Style * IconFont  "$[infostore.font.small.shadow]"

# Window Decoration
#--------------------------------------------------------------------------
TitleStyle	Centered MinHeight 22 -- Flat

ButtonStyle 	Reset
ButtonStyle	all	-- Flat

ButtonStyle 1 ActiveUp Vector 4 35x35@3 65x65@3 70x30@4 35x65@3 -- Flat
ButtonStyle 1 ActiveDown Vector 4 40x40@1 60x60@1 65x35@4 40x60@1 -- Flat
ButtonStyle 1 InactiveUp Vector 4 35x35@3 65x65@3 70x30@4 35x65@3 -- Flat
ButtonStyle 1 InactiveDown Vector 4 40x40@0 60x60@0 65x35@4 40x60@0 -- Flat
ButtonStyle 2 ActiveUp Vector 5 30x70@3 30x30@3 70x30@3 70x70@3 30x70@3 -- Flat
ButtonStyle 2 ActiveDown Vector 5 35x65@1 35x35@1 65x35@1 65x65@1 35x65@1 -- Flat
ButtonStyle 2 ToggledActive Vector 5 35x65@0 35x35@0 65x35@0 65x65@0 35x65@0 -- Flat
ButtonStyle 2 Inactive Vector 5 30x70@3 30x30@3 70x30@3 70x70@3 30x70@3 -- Flat
ButtonStyle 2 ToggledInActive Vector 5 35x65@1 35x35@1 65x35@1 65x65@1 35x65@1 -- Flat
ButtonStyle 4 ActiveUp Vector 5 35x70@3 65x70@3 65x60@3 35x60@3 35x70@3 -- Flat
ButtonStyle 4 ActiveDown Vector 5 40x70@1 65x70@1 65x60@1 40x60@1 40x70@1 -- Flat
ButtonStyle 4 Inactive Vector 5 35x70@3 65x70@3 65x60@3 35x60@3 35x70@3 -- Flat


BorderStyle	Active -- HiddenHandles NoInset Raised
BorderStyle	Inactive -- HiddenHandles NoInset Raised

ButtonStyle 2 - MwmDecorMax
ButtonStyle 4 - MwmDecorMin

DestroyDecor PanelDecor
AddToDecor PanelDecor
+ TitleStyle	MinHeight 22 Raised
+ BorderStyle	Active -- HiddenHandles NoInset Raised 
+ BorderStyle	Inactive -- HiddenHandles NoInset Raised
+ ButtonStyle 4 Active Vector 5 35x70@3 65x70@3 65x60@3 35x60@3 35x70@3 -- UseTitleStyle
+ ButtonStyle 4 Inactive Vector 5 35x70@3 65x70@3 65x60@3 35x60@3 35x70@3 -- UseTitleStyle

Style * FvwmBorder, FvwmButtons 
Style Fvwm* !StickyStippledTitle

# Menus
#--------------------------------------------------------------------------
MenuStyle * Fvwm
MenuStyle * Hilight3DThickness 0, PopupOffset 0 98, PopupDelay 300
MenuStyle * Font "$[infostore.font.normal.shadow]"
MenuStyle * TitleFont "$[infostore.font.normal.shadow]"
MenuStyle * MenuColorset 5, ActiveColorset 6, GreyedColorset 7, TitleColorset 8 
MenuStyle * MenuFace Solid #303541
MenuStyle * HilightBack, HilightTitleBack
MenuStyle * BorderWidth 1, TitleUnderlines1, Animation
MenuStyle * TrianglesUseFore, !TrianglesRelief
MenuStyle * ItemFormat "%1p%s%|%4p%i  %l  %r  %i%>%4p%|%1p"
MenuStyle * VerticalItemSpacing 5 6, VerticalTitleSpacing 5 3
MenuStyle * SeparatorsLong
MenuStyle * VerticalMargins 1 1

# Menu for Backgrounds
CopyMenuStyle * bgMenu
MenuStyle bgMenu VerticalItemSpacing 2 6, VerticalTitleSpacing 4 3
MenuStyle bgMenu Hilight3DThickness 0
MenuStyle bgMenu ActiveColorset 6, HilightBack, MenuColorset 9
MenuStyle bgMenu ItemFormat "%s%|%i  %l%6p%|%"
MenuStyle bgMenu VerticalMargins 0 0

# Icons
#--------------------------------------------------------------------------
Style * IconBackgroundRelief 1, IconTitleRelief 3
Style * IconBackgroundColorset 10, IconTitleColorset 11, HilightIconTitleColorset 9
Style * IconSize 72 62, IconTitle, IconBackgroundPadding 4
Style * IconBox 10 10 -10 -35, IconGrid 23 26, IconFill top right
#left bottom
Style * IconOverride
Style * StickyIcon

#DefaultIcon default.png

#------------------------------------------------------------------------------
# Window Mouse Bindings
#------------------------------------------------------------------------------

#Mouse 1  1  A  Menu "DynamicWindowOps" 
#Mouse 2  1  A  Destroy
#Mouse 3  1  A  Function "CloseWindow"
Mouse 1  1  A  Function "CloseWindow"
Mouse 2  1  A  Destroy
Mouse 3  1  A  WindowList (CurrentPage) NoGeometry, NoCurrentDeskTitle, IconifiedAtEnd
Mouse 1  2  A  Function "MaximizeWindow"
Mouse 2  2  A  MaximizeToSide -0
Mouse 3  2  A  MaximizeToSide +0
Mouse 1  4  A  Function "IconifyWindow"
Mouse 3  4  A  All ($c) Iconify On

Mouse 1  T  A  Function "MoveOrRaiseOrShade"
Mouse 2  T  A  Function "RaiseLowerWindow"
Mouse 3  T  A  Menu "DynamicWindowOps" 

Mouse 3  I  A  Menu "DynamicIconWindowOps" Icon -100m 0
