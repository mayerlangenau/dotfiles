;; -*- lexical-binding: t; -*-

(setq gc-cons-threshold (* 4 1024 1024))
(setq gc-cons-percentage 0.3)

;;; UTF-8
(set-default-coding-systems 'utf-8)
(prefer-coding-system 'utf-8)
(set-language-environment "UTF-8")
(setq locale-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)

;;; No scratch message
(setq initial-scratch-message "")

;;; Fix scrolling
(setq mouse-wheel-progressive-speed nil)
(setq scroll-margin 3)
(setq scroll-preserve-screen-position 'always)
(setq pixel-scroll-precision-large-scroll-height 40.0)
(setq pixel-scroll-precision-interpolation-factor 30)

;; Disable GConf
(define-key special-event-map [config-changed-event] 'ignore)

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

(require 'package)
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(use-package css-mode
  :mode "\\.css\\'")

(use-package markdown-mode
  :ensure t)

(use-package lua-mode
  :ensure t
  :mode "\\.lua\\'")

(use-package latex
    :mode
    ("\\.tex\\'" . latex-mode)
    :bind (("C-<tab>" . TeX-complete-symbol))  
    )

(use-package base16-theme
  :ensure t)

(use-package smart-tabs-mode
  :ensure t
  :config
  (smart-tabs-insinuate 'c 'c++ 'java 'javascript 'cperl 'python
                       'ruby 'nxml)
)

;; Rust
(use-package rust-mode
  :ensure t
  :init
  (setq rust-format-on-save t)
)

;; Python
(use-package python-mode
  :ensure t
  :config
  (setq python-shell-interpreter "/usr/bin/python3")
  (setq indent-tabs-mode t)
  (setq python-indent 4)
  (setq tab-width 4)
  :mode "\\.py\\'")

;;(load-theme 'slack-dark)
;; (add-to-list 'default-frame-alist '(alpha . (99 . 90)))

;; (load "server")
;; (unless (server-running-p) (and window-system (server-start)))

(cua-mode 1)

;;(load-theme 'slack-dark t)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode 1)

(global-hl-line-mode +1)
(show-paren-mode 1)
(setq show-paren-delay 0)

(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;; recent files -> helm-recentf

(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
;;(global-set-key "\C-x\ \C-r" 'recentf-open-files)
(add-hook 'find-file-hook 'recentf-save-list)

(setq scroll-conservatively 101)

(use-package rainbow-delimiters
  :ensure t
  :init
  (progn
    (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
    (add-hook 'markdown-mode-hook 'rainbow-delimiters-mode)))

;; ivy
(use-package ivy
  :diminish ivy-mode
  :ensure t
  :init
  (setq ivy-count-format "(%d/%d) ")
  (setq ivy-use-virtual-buffers t)
  :config (ivy-mode))

(use-package counsel
  :after ivy
  :ensure t
  :demand t
  :bind (("C-x B" . ivy-switch-buffer-other-window)
         ("C-x b" . ivy-switch-buffer)
         ("M-x" . counsel-M-x)
         ("C-x C-r" . counsel-recentf)
         ("C-x C-f" . counsel-find-file)
         ("C-c b" . counsel-bookmark)
         ("<f1> f" . counsel-describe-function)
         ("<f1> v" . counsel-describe-variable))
  :config (counsel-mode))

(use-package swiper
  :ensure t
  :after ivy
  :bind (("C-s" . swiper-isearch)
         ("C-r" . swiper)))

(use-package projectile
  :ensure t
  :pin melpa-stable
  :init
  (projectile-global-mode 1)
  :bind (:map projectile-mode-map
              ("C-c p" . projectile-command-map))
  :config
  (setq projectile-enable-caching t)
  (setq projectile-completion-system 'ivy)
  (setq projectile-indexing-method 'alien))

(projectile-register-project-type 'npm '("package.json")
                  :project-file "package.json"
				  :compile "npm run build"
				  :run "npm start")

;; dashboard
(use-package dashboard
  :ensure t
  :init
  (setq dashboard-set-heading-icons t)
  (setq dashboard-startup-banner 'logo)
  ;;(setq dashboard-set-file-icons t)
  (setq dashboard-footer-messages nil)
  (setq dashboard-items '((recents . 10)
                          (bookmarks . 3)))
  :if (< (length command-line-args) 2)
  :config
  (dashboard-setup-startup-hook)
  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*"))))

(setq inhibit-startup-screen t)

;; treemacs
(use-package treemacs
  :ensure t
  :bind (("<f8>" . treemacs))
  :defer
  :config
  (setq treemacs-show-hidden-files nil)
  (setq treemacs-no-png-images t)
  (setq treemacs-set-width 37)
)


;; all the icons
(use-package all-the-icons
  :ensure t)

(use-package doom-modeline
  :ensure t
  :init
  (doom-modeline-mode 1)
  :custom    
  (doom-modeline-height 25)
  (doom-modeline-bar-width 1)
  (doom-modeline-icon t))

;;(setq doom-modeline-icon (display-graphic-p))
             
;; auto complete
(use-package company
  :ensure t
  :defer t
  :init (global-company-mode)
  :config
  (progn
    ;; Use Company for completion
    (bind-key [remap completion-at-point] #'company-complete company-mode-map)

    (setq company-tooltip-align-annotations t
          ;; Easy navigation to candidates with M-<n>
          company-show-numbers t)
    (setq company-minimum-prefix-length 2)
    (setq company-dabbrev-downcase nil))
  :diminish company-mode)

(use-package company-ctags
  :ensure t
  :after (company))

;; magit
(use-package magit
  :ensure t
  :commands magit-get-top-dir
  :bind (("C-c g" . magit-status)))

;; org mode

(require 'org)
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))

(global-set-key "\C-cl" #'org-store-link)
(global-set-key "\C-ca" #'org-agenda-list)
(global-set-key (kbd "C-c c") #'org-capture)

(setq org-log-done t)
(setq org-agenda-files (list 	"~/Sync/Todos/inbox.org"
                                "~/Sync/Todos/todos.org"
			     				"~/Sync/Todos/piterion.org"
			     				"~/Sync/Todos/smaragd.org" ))
(setq org-agenda-window-setup 'current-window)
(setq org-default-notes-file "~/Sync/Todos/inbox.org")

;;(require 'org-bullets)
;;(add-hook 'org-mode-hook 'org-bullets-mode)

;; tabs

;;(setq backward-delete-char-untabify-method nil)
(global-set-key (kbd "TAB") 'self-insert-command)
(setq-default 	c-basic-offset 3
			  		tab-width 4
			  		indent-tabs-mode nil) ;;t

;; line numbers
;;(global-display-line-numbers-mode 1)
(visual-line-mode t)
(global-visual-line-mode t)

;;(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "C-x C-b") 'buffer-menu)

(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

(global-set-key (kbd "C-c C-l") 'display-line-numbers-mode)
;; german keyboard
(global-set-key (kbd "C-M-ß") 'indent-region)

;; (global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(defun move-text-internal (arg)
  (cond
   ((and mark-active transient-mark-mode)
    (if (> (point) (mark))
        (exchange-point-and-mark))
    (let ((column (current-column))
          (text (delete-and-extract-region (point) (mark))))
      (forward-line arg)
      (move-to-column column t)
      (set-mark (point))
      (insert text)
      (exchange-point-and-mark)
      (setq deactivate-mark nil)))
   (t
    (let ((column (current-column)))
      (beginning-of-line)
      (when (or (> arg 0) (not (bobp)))
        (forward-line)
        (when (or (< arg 0) (not (eobp)))
          (transpose-lines arg))
        (forward-line -1))
      (move-to-column column t)))))

(defun move-text-down (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines down."
  (interactive "*p")
  (move-text-internal arg))

(defun move-text-up (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines up."
  (interactive "*p")
  (move-text-internal (- arg)))

(provide 'move-text)


(global-set-key [M-up] 'move-text-up)
(global-set-key [M-down] 'move-text-down)

(global-font-lock-mode 1)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Fira Code" :foundry "CTDB" :slant normal :weight normal :height 113 :width normal)))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#000000" "#eb008a" "#37b349" "#f8ca12" "#0e5a94" "#b31e8d" "#0e5a94" "#d0d0d0"])
 '(ansi-term-color-vector
   [unspecified "#000000" "#eb008a" "#37b349" "#f8ca12" "#0e5a94" "#b31e8d" "#0e5a94" "#d0d0d0"])
 '(cua-mode t nil (cua-base))
 '(custom-enabled-themes '(base16-default-dark))
 '(custom-safe-themes
   '("d5d87452e2db156d10019017b744dfcc0cd40deae50817e95b8d1b797221468d" default))
 '(delete-selection-mode 1)
 '(inhibit-startup-screen t)
 '(org-capture-templates
   '(("t" "Todo [inbox]" entry
      (file+headline "~/Sync/Todos/inbox.org" "Tasks")
      "* TODO %i%?
  ADDED: %U" :empty-lines 1)))
 '(package-selected-packages
   '(counsel ivy treemacs-projectile hl-anything helm-org treemacs-magit treemacs helm projectile sr-speedbar graphviz-dot-mode company-web lsp-mode markdown-preview-mode markdown-mode web-mode base16-theme writeroom-mode org-sidebar quelpa-leaf quelpa-use-package auctex fvwm-mode ## geiser-guile ac-geiser geiser ac-etags use-package python-mode magit lua-mode doom-modeline dashboard auto-complete))
 '(ring-bell-function 'ignore)
 '(safe-local-variable-values '((projectile-project-name . "homepage")))
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(visible-bell t))
