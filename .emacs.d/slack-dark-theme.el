;;; slack-dark-theme.el --- Slack Dark color theme
;;
;; Copyright 2016 Jonathan Chu
;;
;; Author: Jonathan Chu <me@jonathanchu.is>
;;
;;; Commentary:
;;
;; An Emacs port of the Atom One Dark theme from Atom.io.
;;
;;; Code:

(deftheme slack-dark
  "Slack Dark - An Emacs port of the Atom One Dark theme from Atom.io.")

(defvar slack-dark-colors-alist
  '(("slack-dark-accent"   . "#528BFF")
    ("slack-dark-fg"       . "#D9D9D9")
    ("slack-dark-bg"       . "#252830")
    ("slack-dark-bg-1"     . "#121417")
    ("slack-dark-bg-hl"    . "#2F343D")
    ("slack-dark-mono-1"   . "#ABB2BF")
    ("slack-dark-mono-2"   . "#828997")
    ("slack-dark-mono-3"   . "#5C6370")
    ("slack-dark-cyan"     . "#99C5DA")
    ("slack-dark-blue"     . "#66A2D4")
    ("slack-dark-purple"   . "#8C6EC9")
    ("slack-dark-green"    . "#7DC17F")
    ("slack-dark-red-1"    . "#DA99A9")
    ("slack-dark-red-2"    . "#B05066")
    ("slack-dark-orange-1" . "#D19A66")
    ("slack-dark-orange-2" . "#E5C07B")
    ("slack-dark-gray"     . "#3E4451")
    ("slack-dark-silver"   . "#AAAAAA")
    ("slack-dark-black"    . "#0F1011"))
  "List of Atom One Dark colors.")

(defmacro slack-dark-with-color-variables (&rest body)
  "Bind the colors list around BODY."
  (declare (indent 0))
  `(let ((class '((class color) (min-colors 89)))
         ,@ (mapcar (lambda (cons)
                      (list (intern (car cons)) (cdr cons)))
                    slack-dark-colors-alist))
     ,@body))

(slack-dark-with-color-variables
  (custom-theme-set-faces
   'slack-dark

   `(default ((t (:foreground ,slack-dark-fg :background ,slack-dark-bg))))
   `(success ((t (:foreground ,slack-dark-green))))
   `(warning ((t (:foreground ,slack-dark-orange-2))))
   `(error ((t (:foreground ,slack-dark-red-1 :weight bold))))
   `(link ((t (:foreground ,slack-dark-blue :underline t :weight bold))))
   `(link-visited ((t (:foreground ,slack-dark-blue :underline t :weight normal))))
   `(cursor ((t (:background ,slack-dark-accent))))
   `(fringe ((t (:background ,slack-dark-bg))))
   `(region ((t (:background ,slack-dark-gray))))
   `(highlight ((t (:background ,slack-dark-gray))))
   `(hl-line ((t (:background ,slack-dark-bg-hl))))
   `(vertical-border ((t (:foreground ,slack-dark-mono-3))))
   `(secondary-selection ((t (:background ,slack-dark-bg-1))))
   `(query-replace ((t (:inherit (isearch)))))
   `(minibuffer-prompt ((t (:foreground ,slack-dark-silver))))

   `(font-lock-builtin-face ((t (:foreground ,slack-dark-cyan))))
   `(font-lock-comment-face ((t (:foreground ,slack-dark-mono-3))))
   `(font-lock-comment-delimiter-face ((default (:inherit (font-lock-comment-face)))))
   `(font-lock-doc-face ((t (:inherit (font-lock-string-face)))))
   `(font-lock-function-name-face ((t (:foreground ,slack-dark-blue))))
   `(font-lock-keyword-face ((t (:foreground ,slack-dark-purple))))
   `(font-lock-preprocessor-face ((t (:foreground ,slack-dark-mono-2))))
   `(font-lock-string-face ((t (:foreground ,slack-dark-green))))
   `(font-lock-type-face ((t (:foreground ,slack-dark-orange-2))))
   `(font-lock-constant-face ((t (:foreground ,slack-dark-orange-1))))
   `(font-lock-variable-name-face ((t (:foreground ,slack-dark-red-1))))
   `(font-lock-warning-face ((t (:foreground ,slack-dark-mono-3 :bold t))))

   ;; mode-line
   `(mode-line ((t (:background ,slack-dark-black :foreground ,slack-dark-silver))))
   `(mode-line-buffer-id ((t (:weight bold))))
   `(mode-line-emphasis ((t (:weight bold))))
   `(mode-line-inactive ((t (:background ,slack-dark-gray))))

   ;; ido
   `(ido-first-match ((t (:foreground ,slack-dark-purple :weight bold))))
   `(ido-only-match ((t (:foreground ,slack-dark-red-1 :weight bold))))
   `(ido-subdir ((t (:foreground ,slack-dark-blue))))
   `(ido-virtual ((t (:foreground ,slack-dark-mono-3))))

   ;; ace-jump
   `(ace-jump-face-background ((t (:foreground ,slack-dark-mono-3 :background ,slack-dark-bg-1 :inverse-video nil))))
   `(ace-jump-face-foreground ((t (:foreground ,slack-dark-red-1 :background ,slack-dark-bg-1 :inverse-video nil))))

   ;; company-mode
   `(company-tooltip ((t (:foreground ,slack-dark-fg :background ,slack-dark-bg-1))))
   `(company-tooltip-annotation ((t (:foreground ,slack-dark-mono-2 :background ,slack-dark-bg-1))))
   `(company-tooltip-selection ((t (:foreground ,slack-dark-fg :background ,slack-dark-gray))))
   `(company-tooltip-mouse ((t (:background ,slack-dark-gray))))
   `(company-tooltip-common ((t (:foreground ,slack-dark-orange-2 :background ,slack-dark-bg-1))))
   `(company-tooltip-common-selection ((t (:foreground ,slack-dark-orange-2 :background ,slack-dark-gray))))
   `(company-preview ((t (:background ,slack-dark-bg))))
   `(company-preview-common ((t (:foreground ,slack-dark-orange-2 :background ,slack-dark-bg))))
   `(company-scrollbar-fg ((t (:background ,slack-dark-mono-1))))
   `(company-scrollbar-bg ((t (:background ,slack-dark-bg-1))))

   ;; compilation
   `(compilation-face ((t (:foreground ,slack-dark-fg))))
   `(compilation-line-number ((t (:foreground ,slack-dark-mono-2))))
   `(compilation-column-number ((t (:foreground ,slack-dark-mono-2))))

   ;; isearch
   `(isearch ((t (:foreground ,slack-dark-bg :background ,slack-dark-purple))))
   `(isearch-fail ((t (:foreground ,slack-dark-red-2 :background nil))))
   `(lazy-highlight ((t (:foreground ,slack-dark-purple :background ,slack-dark-bg-1 :underline ,slack-dark-purple))))

   ;; diff-hl (https://github.com/dgutov/diff-hl)
   '(diff-hl-change ((t (:foreground "#E9C062" :background "#8b733a"))))
   '(diff-hl-delete ((t (:foreground "#CC6666" :background "#7a3d3d"))))
   '(diff-hl-insert ((t (:foreground "#A8FF60" :background "#547f30"))))

   ;; dired-mode
   '(dired-directory ((t (:inherit (font-lock-keyword-face)))))
   '(dired-flagged ((t (:inherit (diff-hl-delete)))))
   '(dired-symlink ((t (:foreground "#FD5FF1"))))

   ;; helm
   `(helm-header ((t (:foreground ,slack-dark-mono-2
                      :background ,slack-dark-bg
                      :underline nil
                      :box (:line-width 6 :color ,slack-dark-bg)))))
   `(helm-source-header ((t (:foreground ,slack-dark-orange-2
                             :background ,slack-dark-bg
                             :underline nil
                             :weight bold
                             :box (:line-width 6 :color ,slack-dark-bg)))))
   `(helm-selection ((t (:background ,slack-dark-gray))))
   `(helm-selection-line ((t (:background ,slack-dark-gray))))
   `(helm-visible-mark ((t (:foreground ,slack-dark-bg :foreground ,slack-dark-orange-2))))
   `(helm-candidate-number ((t (:foreground ,slack-dark-green :background ,slack-dark-bg-1))))
   `(helm-separator ((t (:background ,slack-dark-bg :foreground ,slack-dark-red-1))))
   `(helm-M-x-key ((t (:foreground ,slack-dark-orange-1))))
   `(helm-bookmark-addressbook ((t (:foreground ,slack-dark-orange-1))))
   `(helm-bookmark-directory ((t (:foreground nil :background nil :inherit helm-ff-directory))))
   `(helm-bookmark-file ((t (:foreground nil :background nil :inherit helm-ff-file))))
   `(helm-bookmark-gnus ((t (:foreground ,slack-dark-purple))))
   `(helm-bookmark-info ((t (:foreground ,slack-dark-green))))
   `(helm-bookmark-man ((t (:foreground ,slack-dark-orange-2))))
   `(helm-bookmark-w3m ((t (:foreground ,slack-dark-purple))))
   `(helm-match ((t (:foreground ,slack-dark-orange-2))))
   `(helm-ff-directory ((t (:foreground ,slack-dark-cyan :background ,slack-dark-bg :weight bold))))
   `(helm-ff-file ((t (:foreground ,slack-dark-fg :background ,slack-dark-bg :weight normal))))
   `(helm-ff-executable ((t (:foreground ,slack-dark-green :background ,slack-dark-bg :weight normal))))
   `(helm-ff-invalid-symlink ((t (:foreground ,slack-dark-red-1 :background ,slack-dark-bg :weight bold))))
   `(helm-ff-symlink ((t (:foreground ,slack-dark-orange-2 :background ,slack-dark-bg :weight bold))))
   `(helm-ff-prefix ((t (:foreground ,slack-dark-bg :background ,slack-dark-orange-2 :weight normal))))
   `(helm-buffer-not-saved ((t (:foreground ,slack-dark-red-1))))
   `(helm-buffer-process ((t (:foreground ,slack-dark-mono-2))))
   `(helm-buffer-saved-out ((t (:foreground ,slack-dark-fg))))
   `(helm-buffer-size ((t (:foreground ,slack-dark-mono-2))))
   `(helm-buffer-directory ((t (:foreground ,slack-dark-purple))))
   `(helm-grep-cmd-line ((t (:foreground ,slack-dark-cyan))))
   `(helm-grep-file ((t (:foreground ,slack-dark-fg))))
   `(helm-grep-finish ((t (:foreground ,slack-dark-green))))
   `(helm-grep-lineno ((t (:foreground ,slack-dark-mono-2))))
   `(helm-grep-finish ((t (:foreground ,slack-dark-red-1))))
   `(helm-grep-match ((t (:foreground nil :background nil :inherit helm-match))))

   ;; git-commit
   `(git-commit-comment-action  ((t (:foreground ,slack-dark-green :weight bold))))
   `(git-commit-comment-branch  ((t (:foreground ,slack-dark-blue :weight bold))))
   `(git-commit-comment-heading ((t (:foreground ,slack-dark-orange-2 :weight bold))))

   ;; magit
   `(magit-section-highlight ((t (:background ,slack-dark-bg-hl))))
   `(magit-section-heading ((t (:foreground ,slack-dark-orange-2 :weight bold))))
   `(magit-section-heading-selection ((t (:foreground ,slack-dark-fg :weight bold))))
   `(magit-diff-file-heading ((t (:weight bold))))
   `(magit-diff-file-heading-highlight ((t (:background ,slack-dark-gray :weight bold))))
   `(magit-diff-file-heading-selection ((t (:foreground ,slack-dark-orange-2 :background ,slack-dark-bg-hl :weight bold))))
   `(magit-diff-hunk-heading ((t (:foreground ,slack-dark-mono-2 :background ,slack-dark-gray))))
   `(magit-diff-hunk-heading-highlight ((t (:foreground ,slack-dark-mono-1 :background ,slack-dark-mono-3))))
   `(magit-diff-hunk-heading-selection ((t (:foreground ,slack-dark-purple :background ,slack-dark-mono-3))))
   `(magit-diff-context ((t (:foreground ,slack-dark-fg))))
   `(magit-diff-context-highlight ((t (:background ,slack-dark-bg-1 :foreground ,slack-dark-fg))))
   `(magit-diffstat-added ((t (:foreground ,slack-dark-green))))
   `(magit-diffstat-removed ((t (:foreground ,slack-dark-red-1))))
   `(magit-process-ok ((t (:foreground ,slack-dark-green))))
   `(magit-process-ng ((t (:foreground ,slack-dark-red-1))))
   `(magit-log-author ((t (:foreground ,slack-dark-orange-2))))
   `(magit-log-date ((t (:foreground ,slack-dark-mono-2))))
   `(magit-log-graph ((t (:foreground ,slack-dark-silver))))
   `(magit-sequence-pick ((t (:foreground ,slack-dark-orange-2))))
   `(magit-sequence-stop ((t (:foreground ,slack-dark-green))))
   `(magit-sequence-part ((t (:foreground ,slack-dark-orange-1))))
   `(magit-sequence-head ((t (:foreground ,slack-dark-blue))))
   `(magit-sequence-drop ((t (:foreground ,slack-dark-red-1))))
   `(magit-sequence-done ((t (:foreground ,slack-dark-mono-2))))
   `(magit-sequence-onto ((t (:foreground ,slack-dark-mono-2))))
   `(magit-bisect-good ((t (:foreground ,slack-dark-green))))
   `(magit-bisect-skip ((t (:foreground ,slack-dark-orange-1))))
   `(magit-bisect-bad ((t (:foreground ,slack-dark-red-1))))
   `(magit-blame-heading ((t (:background ,slack-dark-bg-1 :foreground ,slack-dark-mono-2))))
   `(magit-blame-hash ((t (:background ,slack-dark-bg-1 :foreground ,slack-dark-purple))))
   `(magit-blame-name ((t (:background ,slack-dark-bg-1 :foreground ,slack-dark-orange-2))))
   `(magit-blame-date ((t (:background ,slack-dark-bg-1 :foreground ,slack-dark-mono-3))))
   `(magit-blame-summary ((t (:background ,slack-dark-bg-1 :foreground ,slack-dark-mono-2))))
   `(magit-dimmed ((t (:foreground ,slack-dark-mono-2))))
   `(magit-hash ((t (:foreground ,slack-dark-purple))))
   `(magit-tag  ((t (:foreground ,slack-dark-orange-1 :weight bold))))
   `(magit-branch-remote  ((t (:foreground ,slack-dark-green :weight bold))))
   `(magit-branch-local   ((t (:foreground ,slack-dark-blue :weight bold))))
   `(magit-branch-current ((t (:foreground ,slack-dark-blue :weight bold :box t))))
   `(magit-head           ((t (:foreground ,slack-dark-blue :weight bold))))
   `(magit-refname        ((t (:background ,slack-dark-bg :foreground ,slack-dark-fg :weight bold))))
   `(magit-refname-stash  ((t (:background ,slack-dark-bg :foreground ,slack-dark-fg :weight bold))))
   `(magit-refname-wip    ((t (:background ,slack-dark-bg :foreground ,slack-dark-fg :weight bold))))
   `(magit-signature-good      ((t (:foreground ,slack-dark-green))))
   `(magit-signature-bad       ((t (:foreground ,slack-dark-red-1))))
   `(magit-signature-untrusted ((t (:foreground ,slack-dark-orange-1))))
   `(magit-cherry-unmatched    ((t (:foreground ,slack-dark-cyan))))
   `(magit-cherry-equivalent   ((t (:foreground ,slack-dark-purple))))
   `(magit-reflog-commit       ((t (:foreground ,slack-dark-green))))
   `(magit-reflog-amend        ((t (:foreground ,slack-dark-purple))))
   `(magit-reflog-merge        ((t (:foreground ,slack-dark-green))))
   `(magit-reflog-checkout     ((t (:foreground ,slack-dark-blue))))
   `(magit-reflog-reset        ((t (:foreground ,slack-dark-red-1))))
   `(magit-reflog-rebase       ((t (:foreground ,slack-dark-purple))))
   `(magit-reflog-cherry-pick  ((t (:foreground ,slack-dark-green))))
   `(magit-reflog-remote       ((t (:foreground ,slack-dark-cyan))))
   `(magit-reflog-other        ((t (:foreground ,slack-dark-cyan))))

   ;; rainbow-delimiters
   `(rainbow-delimiters-depth-1-face ((t (:foreground ,slack-dark-fg))))
   `(rainbow-delimiters-depth-2-face ((t (:foreground ,slack-dark-purple))))
   `(rainbow-delimiters-depth-3-face ((t (:foreground ,slack-dark-blue))))
   `(rainbow-delimiters-depth-4-face ((t (:foreground ,slack-dark-cyan))))
   `(rainbow-delimiters-depth-5-face ((t (:foreground ,slack-dark-green))))
   `(rainbow-delimiters-depth-6-face ((t (:foreground ,slack-dark-orange-1))))
   `(rainbow-delimiters-depth-7-face ((t (:foreground ,slack-dark-orange-2))))
   `(rainbow-delimiters-depth-8-face ((t (:foreground ,slack-dark-red-1))))
   `(rainbow-delimiters-depth-9-face ((t (:foreground ,slack-dark-red-2))))
   `(rainbow-delimiters-depth-10-face ((t (:foreground ,slack-dark-mono-1))))
   `(rainbow-delimiters-depth-11-face ((t (:foreground ,slack-dark-mono-2))))
   `(rainbow-delimiters-depth-12-face ((t (:foreground ,slack-dark-mono-3))))
   `(rainbow-delimiters-unmatched-face ((t (:foreground ,slack-dark-black))))

   ;; rbenv
   `(rbenv-active-ruby-face ((t (:foreground ,slack-dark-green))))

   ;; smartparens
   `(sp-show-pair-mismatch-face ((t (:foreground ,slack-dark-red-1 :background ,slack-dark-gray :weight bold))))
   `(sp-show-pair-match-face ((t (:background ,slack-dark-gray :weight bold))))

   ;; web-mode
   `(web-mode-symbol-face ((t (:foreground ,slack-dark-orange-1))))

   ;; flx-ido
   `(flx-highlight-face ((t (:inherit (link) :weight bold))))

   ;; term
   `(term-color-black ((t :foreground ,slack-dark-mono-1)))
   `(term-color-blue ((t (:foreground ,slack-dark-blue))))
   `(term-color-cyan ((t :foreground ,slack-dark-cyan)))
   `(term-color-green ((t (:foreground ,slack-dark-green))))
   `(term-color-magenta ((t :foreground ,slack-dark-purple)))
   `(term-color-red ((t :foreground ,slack-dark-red-1)))
   `(term-color-white ((t :foreground ,slack-dark-fg)))
   `(term-color-yellow ((t (:foreground ,slack-dark-orange-1))))
   ))

;;;###autoload
(and load-file-name
    (boundp 'custom-theme-load-path)
    (add-to-list 'custom-theme-load-path
                 (file-name-as-directory
                  (file-name-directory load-file-name))))
;; Automatically add this theme to the load path

(provide-theme 'slack-dark)

;; Local Variables:
;; no-byte-compile: t
;; End:
;;; slack-dark-theme.el ends here
