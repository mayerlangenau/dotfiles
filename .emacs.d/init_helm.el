;; -*- lexical-binding: t; -*-

(setq gc-cons-threshold (* 4 1024 1024))
(setq gc-cons-percentage 0.3)

;;; UTF-8
(set-default-coding-systems 'utf-8)
(prefer-coding-system 'utf-8)
(set-language-environment "UTF-8")
(setq locale-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)

;;; No scratch message
(setq initial-scratch-message "")

;;; Fix scrolling
(setq mouse-wheel-progressive-speed nil)
(setq scroll-margin 3)
(setq scroll-preserve-screen-position 'always)
(setq pixel-scroll-precision-large-scroll-height 40.0)
(setq pixel-scroll-precision-interpolation-factor 30)

;; Disable GConf
(define-key special-event-map [config-changed-event] 'ignore)

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

(require 'package)
(add-to-list 'package-archives '("melpa-stable" . "http://stable.melpa.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(use-package css-mode
  :mode "\\.css\\'")

(use-package markdown-mode
  :ensure t)

(use-package lua-mode
  :ensure t
  :mode "\\.lua\\'")

(use-package latex
    :mode
    ("\\.tex\\'" . latex-mode)
    :bind (("C-<tab>" . TeX-complete-symbol))  
    )

(use-package base16-theme
  :ensure t)

(use-package smart-tabs-mode
  :ensure t
  :config
  (smart-tabs-insinuate 'c 'c++ 'java 'javascript 'cperl 'python
                       'ruby 'nxml)
)

;; Rust
(use-package rust-mode
  :ensure t
  :init
  (setq rust-format-on-save t)
)

;; Python
(use-package python-mode
  :ensure t
  :config
  (setq python-shell-interpreter "/usr/bin/python3")
  (setq indent-tabs-mode t)
  (setq python-indent 4)
  (setq tab-width 4)
  :mode "\\.py\\'")

;;(load-theme 'slack-dark)
;; (add-to-list 'default-frame-alist '(alpha . (99 . 90)))

;; (load "server")
;; (unless (server-running-p) (and window-system (server-start)))

(cua-mode 1)

;;(load-theme 'slack-dark t)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(menu-bar-mode 1)

(global-hl-line-mode +1)
(show-paren-mode 1)
(setq show-paren-delay 0)

(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;; recent files -> helm-recentf

(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
;;(global-set-key "\C-x\ \C-r" 'recentf-open-files)
(add-hook 'find-file-hook 'recentf-save-list)

(setq scroll-conservatively 101)

(use-package rainbow-delimiters
  :ensure t
  :init
  (progn
    (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
    (add-hook 'markdown-mode-hook 'rainbow-delimiters-mode)))

(use-package helm
  :ensure t
  :pin melpa-stable
  :demand t
  :requires helm-config
)

(use-package helm-mode
  :init
  (add-hook 'helm-mode-hook
            (lambda ()
              (setq completion-styles
                    (cond ((assq 'helm-flex completion-styles-alist)
                           '(helm-flex)) ;; emacs-26.
                          ((assq 'flex completion-styles-alist)
                           '(flex)))))) ;; emacs-27+.
  :diminish (helm-mode " ⎈")
  :bind
  ("M-x" . helm-M-x)
  ("C-x C-f" . helm-find-files)
  ("C-x C-b" . helm-mini)
  ("C-x b" . helm-buffers-list)
  ("C-x C-r" . helm-recentf)
  ("C-x r" . helm-bookmarks)
  ;;("C-s" . helm-occur)
  :config
  (helm-mode 1)
  (setq helm-ff-file-name-history-use-recentf t)
  (define-key global-map [remap list-buffers] 'helm-buffers-list))

(use-package helm-swoop
  :ensure t
  :bind
  ("C-s" . helm-swoop)
  ("C-r" . helm-swoop)
  :config
  (define-key helm-swoop-map (kbd "C-r") 'helm-previous-line)
  (define-key helm-swoop-map (kbd "C-s") 'helm-next-line) 
  (define-key helm-multi-swoop-map (kbd "C-r") 'helm-previous-line)
  (define-key helm-multi-swoop-map (kbd "C-s") 'helm-next-line)
  (setq helm-swoop-split-with-multiple-windows t)
  (setq helm-swoop-speed-or-color nil)
  (setq helm-swoop-pre-input-function (lambda () ""))
)

(use-package projectile
  :ensure t
  :pin melpa-stable
  :init
  (projectile-global-mode 1)
  :bind (:map projectile-mode-map
              ("C-c p" . projectile-command-map))
  :config
  (setq projectile-enable-caching t)
  (setq projectile-completion-system 'helm)
  (setq projectile-indexing-method 'alien))

(projectile-register-project-type 'npm '("package.json")
                  :project-file "package.json"
				  :compile "npm run build"
				  :run "npm start")

;; dashboard
(use-package dashboard
  :ensure t
  :init
  (setq dashboard-set-heading-icons t)
  (setq dashboard-startup-banner 'logo)
  ;;(setq dashboard-set-file-icons t)
  (setq dashboard-footer-messages nil)
  (setq dashboard-items '((recents . 10)
                          (bookmarks . 3)))
  :if (< (length command-line-args) 2)
  :config
  (dashboard-setup-startup-hook)
  (setq initial-buffer-choice (lambda () (get-buffer "*dashboard*"))))

(setq inhibit-startup-screen t)

;; neotree
(use-package treemacs
  :ensure t
  :bind (("<f8>" . treemacs))
  :defer
  :config
  (setq treemacs-show-hidden-files nil)
  (setq treemacs-no-png-images t)
  (setq treemacs-set-width 37)
)


;; all the icons
(use-package all-the-icons
  :ensure t)

(use-package doom-modeline
  :ensure t
  :init
  (doom-modeline-mode 1)
  :custom    
  (doom-modeline-height 25)
  (doom-modeline-bar-width 1)
  (doom-modeline-icon t))

;;(setq doom-modeline-icon (display-graphic-p))
             
;; auto complete
(use-package company
  :ensure t
  :defer t
  :init (global-company-mode)
  :config
  (progn
    ;; Use Company for completion
    (bind-key [remap completion-at-point] #'company-complete company-mode-map)

    (setq company-tooltip-align-annotations t
          ;; Easy navigation to candidates with M-<n>
          company-show-numbers t)
    (setq company-minimum-prefix-length 2)
    (setq company-dabbrev-downcase nil))
  :diminish company-mode)

(use-package company-ctags
  :ensure t
  :after (company))

;; magit
(use-package magit
  :ensure t
  :commands magit-get-top-dir
  :bind (("C-c g" . magit-status)))

;; org mode

(require 'org)
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))

(global-set-key "\C-cl" #'org-store-link)
(global-set-key "\C-ca" #'org-agenda-list)
(global-set-key (kbd "C-c c") #'org-capture)

(setq org-log-done t)
(setq org-agenda-files (list 	"~/Sync/Todos/inbox.org"
                                "~/Sync/Todos/todos.org"
			     				"~/Sync/Todos/piterion.org"
			     				"~/Sync/Todos/smaragd.org" ))
(setq org-agenda-window-setup 'current-window)
(setq org-default-notes-file "~/Sync/Todos/inbox.org")

;;(require 'org-bullets)
;;(add-hook 'org-mode-hook 'org-bullets-mode)

;; tabs

;;(setq backward-delete-char-untabify-method nil)
(global-set-key (kbd "TAB") 'self-insert-command)
(setq-default 	c-basic-offset 3
			  		tab-width 4
			  		indent-tabs-mode nil) ;;t

;; line numbers
;;(global-display-line-numbers-mode 1)
(visual-line-mode t)
(global-visual-line-mode t)

;;(global-set-key (kbd "C-x C-b") 'ibuffer)
;(global-set-key (kbd "C-x C-b") 'buffer-menu)

;(global-set-key (kbd "C-x r b") #'helm-filtered-bookmarks)

(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

(global-set-key (kbd "C-c C-l") 'display-line-numbers-mode)
;; german keyboard
(global-set-key (kbd "C-M-ß") 'indent-region)

;; (global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(defun move-text-internal (arg)
  (cond
   ((and mark-active transient-mark-mode)
    (if (> (point) (mark))
        (exchange-point-and-mark))
    (let ((column (current-column))
          (text (delete-and-extract-region (point) (mark))))
      (forward-line arg)
      (move-to-column column t)
      (set-mark (point))
      (insert text)
      (exchange-point-and-mark)
      (setq deactivate-mark nil)))
   (t
    (let ((column (current-column)))
      (beginning-of-line)
      (when (or (> arg 0) (not (bobp)))
        (forward-line)
        (when (or (< arg 0) (not (eobp)))
          (transpose-lines arg))
        (forward-line -1))
      (move-to-column column t)))))

(defun move-text-down (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines down."
  (interactive "*p")
  (move-text-internal arg))

(defun move-text-up (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines up."
  (interactive "*p")
  (move-text-internal (- arg)))

(provide 'move-text)

(global-set-key [M-up] 'move-text-up)
(global-set-key [M-down] 'move-text-down)

(defun my-buffer-predicate (buffer)
  (if (string-match "helm" (buffer-name buffer))
      nil
    t))
(set-frame-parameter nil 'buffer-predicate 'my-buffer-predicate)

(global-font-lock-mode 1)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Cascadia Mono" :foundry "SAJA" :slant normal :weight normal :height 113 :width normal))))
 '(neo-dir-link-face ((t (:foreground "#b294bb"))))
 '(neo-expand-btn-face ((t (:foreground "#81a2be"))))
 '(neo-file-link-face ((t (:foreground "#c5c8c6"))))
 '(neo-root-dir-face ((t (:foreground "#81a2be")))))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#000000" "#eb008a" "#37b349" "#f8ca12" "#0e5a94" "#b31e8d" "#0e5a94" "#d0d0d0"])
 '(ansi-term-color-vector
   [unspecified "#000000" "#eb008a" "#37b349" "#f8ca12" "#0e5a94" "#b31e8d" "#0e5a94" "#d0d0d0"])
 '(cua-mode t nil (cua-base))
 '(custom-enabled-themes '(base16-tomorrow-night))
 '(custom-safe-themes
   '("331f3231988bb83403775f8a1c66a4512a18b527c38e30df3ec3a6e63fad8ae2" "1263771faf6967879c3ab8b577c6c31020222ac6d3bac31f331a74275385a452" "d178e2074d5a008f8fe633b4ae7423cbd062b481af298ab6f4dc2d7c1b8fb44f" "66301c9ab82788101de4787149c97c34baf22f37663bf2a21aed5b46242707d0" "c9c9c51b21e8054c872a4c421641c6490a2a5239c4d2d67b0f1302512f9b04b2" "80fdbf1fa7f994f7fe7ac6fdb821888153f0c276da6b3840de3c520bddd119a3" "218d32b8bab3320e4777051e311dfcb57d1902aadcf4b9aa26e94f45f33bcfbc" "a5925b2850e78f86339478a32d31103cd802d736c5e2e62538e7a89564bb2168" "10dfc6109cbacfe118219f8e2522eadc0beff9418b30f99f8b4e28b24b24450a" "c174371ed220c87ca602ea22240c78e269f0a6acee409efc3d42ba543eed4b78" "7b70b7adbd3f45ec8656660ddcd7bdee728dac382a52b13e649dbd66e066cffd" "b15473f0bb1d124cd02f356715b63e1af8386f1c93d1bf8b0b315b04d1725225" "0a99f5b1d80ac79f837c8b493e0a7105e75d1bf71eb5d241cdab348cf4e35d1e" "ab4445ba203e5e6c0dd099bace7295baaa434f0eaa4639ad1f54cedaa2e6a178" "ca1723c0c7d03bb588b50e215452cc125b458ca1c8e910f59c1e6a41687c840a" "23bd4bfcc6e8d66236d66071bd4c6c4d5d7b1a0f7fd21af5bd048e4896f6f223" "4f287fc0ff60dbc7d6b779dfb4f631567ac3d0371be92bc0f654740aebef9bc5" "6ac414d9fb6a82187972a6f1d1c422323eda97a45c3c0d782154a309ce9ff3fa" "3f0a594094bc7a86c44ceb166df857a780569b44f0ed26ca9ce7cc9c44c74aaa" "34fa1e97f2c103b1408f40608c1d326447c79aa1b2b685b9e6b2b4b443a164da" "36de693fc12bb90d63c659d31f9757f4fd920e9eded545bd6ab12a8f0afbf2b8" "963edc85a9fdf4ba7005a110c586a1166fd77abd1749335ed09bac9eb584137f" "178ab06a38efa8ed13ca660543ae359b9bbe62eb90e44ee2ac3c306022a38e06" "54b2dc8e44f05a2eb77e9ae860d478c14efafbc542428b4831284afbd6a2efcd" "0bf2ee34d3726a0c200372e47265e83e5a1fcd3783426e99870cb763efacf3c9" "97cc742a9df1b49b0aeaa6291a469a70f20942d09864a3a78e6ab9e22f691bda" "578eafed0551eef1203db6180054caa27e6d30fdab465db6dc4c6f1193dc5053" "f127790cdcbb30213579f2ea3ec224fdab78470228df149673b6929122da150d" "f4974f724e635c117116a61ea9951a584be357f815366f989448dd7e3d94b28e" "18985a431529be851c0faca2c8595787b86c41a9f4bf2b3348472adcb98dac9c" "fc86631a52d5a24de87f2accdbcedd59c7e9ac8984c29ec12111d770a1b8cbfc" "ace81edf1562ee13380904330ecffa5a1acddf1e62b8da632cbc211aff176868" "99517cfd0d1c55ea64b066037b092f71541cd63698f2d7814cc832d20fb6a412" "16c55449a43382e184864b2f2d818a0a68696ca721dbb2f1886e6d7c40a5ea8c" "451e57275e15f2d2aed5301e9cefd73cdd22bcbebfbcc5bd5d6ff7735307394f" "f7d783db782492021a3062ced6dcba72e4f0ba4c0cb7522da157c1a4b614ab08" "64234afed47b58176eb8f2ef89af4e07f562de7efe56f713e53195bde12a517e" "97d087707473521bc0f539b252fd7e3de78546d23d303af3f34d8f21ae8fc00f" "6fea6d14e8341de251e44a8fbb17c5749a6ddaf4f59d4e030666add46a9d7daa" "b25472a5617db7767b160f96394c772703641ebd53918f578ee29c41b500ae29" "47673bbc1ce9c3f2f0b6f339e66b01f040211d6d2ee141c30c03f2e6ab104f0d" "e98b6c5129f2c02c10ceade417545cd0b906213ba813b8d227004ce2be8130b9" "9577fba482c47b89d912c5daa0d135cfc5f6358a0430a06b1f896f8e6df654f4" "86fef7d09e269a3e36906121837e87ee5a3db62c6d90dbe848f9df3d6f730154" "47cfbb16b287df65b367bf4c05034e6a304abe6ecf2ca743f418f0eeb8e272a1" "0bf7873aa3c6308cb96201b49694ea4ccc65d3a1505655a7008ffb7a1055c7b8" "d5d87452e2db156d10019017b744dfcc0cd40deae50817e95b8d1b797221468d" "41c8a1c69277145eecf334475c185717b3fd1228ca1b6d71f93afef7402bb75a" "41f7f823fcb006096dfd436b91b9fb680a94b0a181cfff2767e1439123a51fbf" "dfe9e8bb25ee9d21de04d859066e453d5ea21900425eb77ce4a399842cbbb24b" "d48edf70ed6e27de5cb77fa600a8946f3ee8875212ed6dcfdbe969e7b1997e20" "25235204c5c77432ee5410157eaaaef9efbb081cdde81613910439ae01f9662e" "f7c1dc9d214b0a83500c17293300ec20fbb2f14cb538925d28dcdcbf76f05e35" "adb099304454c02a27365038ae23d22de875e2e4fe3ba6851cb84c54ea072aa3" "7291a19601fe0d286b7d541e535ed27f36c6399628644551f7d8770bcc8e7709" "a97502de4d61b00c22a4c0acb7b0d564fe72a2b6314ea96522775e58f53be5be" "3a29521c12eda96ca5ecf05ca053e864939a4bedc934c2868a960e7a6b874c93" "bcf435d3255cc7c50a73e1d9606f9a8d1812b236e941a4a98aa82dea526afa8c" "4d99d5745b4ea1e57faa3e2a3e775a266f4da454cf5b75bf1d640686d32d9d16" "bd8ac87046d9ba8016159219097807d40a8fd4a794605a5db4fd39c5f08c1a2e" "f38e2ba95883ae46d3682d646ca358db303853bf8bdbb9870570c280c1702e84" "e379fec18db50ef5a42cfbdff5eca4ad3cfff4a65e14d4a2fbcb873a8be32dcb" "00160bcd6ef6effe580927d9166e65d763feee5952e4f76c341a6f088a6a4c3c" "addd88a26e5c7565601cfd1befa0ba84889ac5dfe010ddfcde99a8738003bec5" "b6d43dd81d3da40ea76df467365351b3dfd2b6f7ca1d6b6439aad43b69c8ff3e" "ee7720a00b632d2eaec599e2c3ec71606dafe008ce020e48baf9fb0abddcb0a3" "fc6eb1cb01d3f759b30c10bbf00f6e6b0c4ed3b8c82479d6afbf01955c114b40" "539a0f074a0825c9361587468b39760926670fecdf08aed863ee1051e8da04d0" "e027e0859bc055f650e10fd1de1940988afecc430e37c05927364c3564787918" "f92893021b9da7d701d50418721b6ba7c42264ac7630b4da6054621cc73f53b2" "b2c48d480e0a5768b688637703b010d3351d09de5f7c500793938c8cdf77d3f5" "a2bfe26945dd332b89d3fb2165510c28f8cb1df9c85e5ee78a38f5755ee9dc6f" "c3a3467a198b948177947063a5a5ec954598f21d6b400c83c85f7f58db2f0c45" "b5703803e5da4b8e33b8c34282f843677f6537c51092fe74ce1b48052042650d" "3cddc7857469dfa0697eae93ad0a214e6894ee9f3fe9979d224c4bcd835b2f8d" "87d05926dca5106a5eac77fd738e150a647fb66a0f86cb8ceaa64717f8d1090a" "23031b55e82bcc6d3d8a689d9c0c6d595676f7be1ca5f894b0d87e57df2a47a6" "5b25fa35523f4958e6086bd823b7eb9c44aae4bc9c99d8b265fe11205df27779" "305de0d24f107a99255859e4f14bc1aa9920799df593377a960da401fb272662" "f195642024c1a2923400c39df9389f9d5c4dff5073c9fe5cf4aa6785e4739449" "a6a75d921a28088484a059d64220f3d3c41f6bbbfa91fe154cbe9019256023a3" "594df9401f0f37916120d7afe00bd7cc80025eb548cf1e19075ce3845b2f84ae" "b8929cff63ffc759e436b0f0575d15a8ad7658932f4b2c99415f3dde09b32e97" "5a7830712d709a4fc128a7998b7fa963f37e960fd2e8aa75c76f692b36e6cf3c" "c968804189e0fc963c641f5c9ad64bca431d41af2fb7e1d01a2a6666376f819c" "3de3f36a398d2c8a4796360bfce1fa515292e9f76b655bb9a377289a6a80a132" "c9f102cf31165896631747fd20a0ca0b9c64ecae019ce5c2786713a5b7d6315e" "146061a7ceea4ccc75d975a3bb41432382f656c50b9989c7dc1a7bb6952f6eb4" "36282815a2eaab9ba67d7653cf23b1a4e230e4907c7f110eebf3cdf1445d8370" "93268bf5365f22c685550a3cbb8c687a1211e827edc76ce7be3c4bd764054bad" "aea30125ef2e48831f46695418677b9d676c3babf43959c8e978c0ad672a7329" "12670281275ea7c1b42d0a548a584e23b9c4e1d2dabb747fd5e2d692bcd0d39b" "50d07ab55e2b5322b2a8b13bc15ddf76d7f5985268833762c500a90e2a09e7aa" "3380a2766cf0590d50d6366c5a91e976bdc3c413df963a0ab9952314b4577299" "cea3ec09c821b7eaf235882e6555c3ffa2fd23de92459751e18f26ad035d2142" "722e1cd0dad601ec6567c32520126e42a8031cd72e05d2221ff511b58545b108" "7bef2d39bac784626f1635bd83693fae091f04ccac6b362e0405abf16a32230c" "6daa09c8c2c68de3ff1b83694115231faa7e650fdbb668bc76275f0f2ce2a437" "4feee83c4fbbe8b827650d0f9af4ba7da903a5d117d849a3ccee88262805f40d" "196df8815910c1a3422b5f7c1f45a72edfa851f6a1d672b7b727d9551bb7c7ba" "6145e62774a589c074a31a05dfa5efdf8789cf869104e905956f0cbd7eda9d0e" "16dd114a84d0aeccc5ad6fd64752a11ea2e841e3853234f19dc02a7b91f5d661" "4bf5c18667c48f2979ead0f0bdaaa12c2b52014a6abaa38558a207a65caeb8ad" default))
 '(delete-selection-mode 1)
 '(inhibit-startup-screen t)
 '(org-capture-templates
   '(("t" "Todo [inbox]" entry
      (file+headline "~/Sync/Todos/inbox.org" "Tasks")
      "* TODO %i%?
  ADDED: %U" :empty-lines 1)))
 '(package-selected-packages
   '(popwin counsel ivy treemacs-projectile hl-anything helm-org treemacs-magit treemacs helm projectile sr-speedbar graphviz-dot-mode company-web lsp-mode markdown-preview-mode markdown-mode web-mode base16-theme writeroom-mode org-sidebar quelpa-leaf quelpa-use-package auctex fvwm-mode ## geiser-guile ac-geiser geiser ac-etags use-package python-mode magit lua-mode doom-modeline dashboard auto-complete))
 '(ring-bell-function 'ignore)
 '(safe-local-variable-values '((projectile-project-name . "homepage")))
 '(show-paren-mode t)
 '(tool-bar-mode nil)
 '(visible-bell t))
