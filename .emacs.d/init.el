;; -*- lexical-binding: t; -*-

(setq gc-cons-threshold (* 4 1024 1024))
(setq gc-cons-percentage 0.3)

;;; UTF-8
(set-default-coding-systems 'utf-8)
(prefer-coding-system 'utf-8)
(set-language-environment "UTF-8")
(setq locale-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)

;;; No scratch message
(setq initial-scratch-message "")

;;; Fix scrolling
(setq mouse-wheel-progressive-speed nil)
(setq scroll-margin 3)
(setq scroll-preserve-screen-position 'always)
(setq pixel-scroll-precision-large-scroll-height 40.0)
(setq pixel-scroll-precision-interpolation-factor 30)
(setq scroll-conservatively 101)

;; Disable GConf
(define-key special-event-map [config-changed-event] 'ignore)

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("elpa" . "https://elpa.gnu.org/packages/"))
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(eval-and-compile
  (setq use-package-always-ensure t
        use-package-expand-minimally t))

(use-package css-mode
  :mode "\\.css\\'")

(use-package markdown-mode)

(use-package lua-mode
  :defer t
  :mode "\\.lua\\'")

(use-package tex
  :ensure auctex
  :mode ("\\.tex\\'" . latex-mode)
  :bind (("C-<tab>" . TeX-complete-symbol))  
)

;;(use-package doom-themes
;;  :config
;;  (load-theme 'doom-tomorrow-night t))

(use-package modus-themes
  :init
  ;; Add all your customizations prior to loading the themes
  (setq modus-themes-italic-constructs t
        modus-themes-disable-other-themes t
        modus-themes-bold-constructs nil
        modus-themes-subtle-line-numbers nil
        modus-themes-mode-line '(borderless)
        modus-themes-hl-line '(intense)
        modus-themes-to-toggle '(modus-operandi modus-vivendi-tinted)
        modus-themes-region '(bg-only no-extend))
  ;; Remove the border
  (setq modus-themes-common-palette-overrides
      '((border-mode-line-active unspecified)
        (border-mode-line-inactive unspecified)))
  ;; Load the theme files before enabling a theme
  (require-theme 'modus-themes) ;
  :config
  ;; Load the theme of your choice:
  (load-theme 'modus-operandi :no-confirm) ;; OR (modus-vivendi) 
  :bind ("<f5>" . modus-themes-toggle))

;; Rust
(use-package rust-mode
  :defer t
  :init
  (setq rust-format-on-save t)
)

;; Python
(use-package python-mode
  :config
  (setq python-shell-interpreter "/usr/bin/python3")
  (setq indent-tabs-mode t)
  (setq python-indent 4)
  (setq tab-width 4)
  :mode "\\.py\\'")

;; (load "server")
;; (unless (server-running-p) (and window-system (server-start)))

(tool-bar-mode -1)
(scroll-bar-mode t)
(menu-bar-mode 1)

(global-hl-line-mode +1)
(show-paren-mode 1)
(setq show-paren-delay 0)

(add-hook 'prog-mode-hook 'display-line-numbers-mode)

(use-package cua-base
  :init (cua-mode 1)
  :config
  (progn
    (setq cua-toggle-set-mark nil)))

(use-package rainbow-delimiters
  :init
  (progn
    (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
    (add-hook 'markdown-mode-hook 'rainbow-delimiters-mode)))

;; ivy
(use-package ivy
  :diminish ivy-mode
  :init
  (setq ivy-count-format "(%d/%d) ")
  (setq ivy-use-virtual-buffers t)
  (setq ivy-use-selectable-prompt t)
  :config (ivy-mode))

(use-package ivy-rich
  :after ivy
  :config (ivy-rich-mode))

(use-package all-the-icons-ivy-rich
  :after ivy
  :init (all-the-icons-ivy-rich-mode 1))

(use-package counsel
  :after ivy
  :diminish
  :demand t
  :bind (("C-x B" . ivy-switch-buffer-other-window)
         ("C-x b" . ivy-switch-buffer)
         ("M-x" . counsel-M-x)
         ("C-x C-r" . counsel-recentf)
         ("C-x C-f" . counsel-find-file)
         ("C-c b" . counsel-bookmark)
         ("<f1> f" . counsel-describe-function)
         ("<f1> v" . counsel-describe-variable))
  :config (counsel-mode))

(use-package amx
  :init
  (setq amx-history-length 15)
  (setq amx-show-key-bindings t)
  :config (amx-initialize))

(use-package swiper
  :after ivy
  :bind (("C-s" . swiper-isearch)
         ("C-r" . swiper)))

(use-package projectile
  :defer t
;  :init
;  (projectile-global-mode 1)
  :bind (:map projectile-mode-map
              ("C-c p" . projectile-command-map))
  :config
  (setq projectile-enable-caching t)
  (setq projectile-completion-system 'ivy)
  (setq projectile-indexing-method 'alien)
  (projectile-register-project-type 'npm '("package.json")
                  :project-file "package.json"
				  :compile "npm run build"
				  :run "npm start"))

;; dashboard
(use-package dashboard
	:init
		(setq dashboard-set-heading-icons nil)
 		(setq dashboard-set-file-icons t)
  		(setq dashboard-startup-banner 1)
		(setq dashboard-items '((recents . 5)
	                            (bookmarks . 3)
                                (agenda . 5)))
	:config
	(dashboard-setup-startup-hook))


(setq inhibit-startup-screen t)

;; treemacs
;;(use-package treemacs
;;  :bind (("<f8>" . treemacs))
;;  :defer
;;  :config
;;  (setq treemacs-show-hidden-files nil)
;;  (setq treemacs-no-png-images t)
;;  (setq treemacs-set-width 35)
;;)

;; all the icons
(use-package all-the-icons
  :if (display-graphic-p))

(use-package doom-modeline
  :init
  (doom-modeline-mode 1)
  :custom    
  (doom-modeline-height 25)
  (doom-modeline-bar-width 1)
  (doom-modeline-icon t))

;;(setq doom-modeline-icon (display-graphic-p))
             
;; auto complete
(use-package company
  :defer t
  :init (global-company-mode)
  :config
  (progn
    ;; Use Company for completion
    (bind-key [remap completion-at-point] #'company-complete company-mode-map)

    (setq company-tooltip-align-annotations t
          ;; Easy navigation to candidates with M-<n>
          company-show-numbers t)
    (setq company-minimum-prefix-length 2)
    (setq company-dabbrev-downcase nil))
  :diminish company-mode)

(use-package company-ctags
  :after (company))

;; magit
(use-package magit
  :commands magit-get-top-dir
  :bind (("C-c g" . magit-status)))

;; org mode
(use-package org
  :mode (("\\.org$" . org-mode))
  :bind (("C-c l" . 'org-store-link)
    ("C-c a" . 'org-agenda)
  )
  :config
  (setq org-log-done t)
  ;; agenda
  (setq org-agenda-files (list 	"~/Dokumente/Roam/inbox.org"
                                "~/Dokumente/Roam/habits.org"
                                "~/Dokumente/Roam/todos.org" ))
  (setq org-agenda-skip-scheduled-if-done t
        org-agenda-window-setup 'current-window
      	org-agenda-skip-deadline-if-done t
      	org-agenda-include-deadlines t
      	org-agenda-block-separator #x2501
      	org-agenda-compact-blocks t
      	org-agenda-start-with-log-mode t)

  (setq-default org-enforce-todo-dependencies t)
  (add-to-list 'org-modules 'org-habit t)
  (setq org-habit-show-habits-only-for-today nil)
)

(use-package org-fancy-priorities
  :after org
  :hook (org-mode . org-fancy-priorities-mode)
  :config
  (setq org-fancy-priorities-list '("HIGH" "MID" "LOW" "OPTIONAL"))
  (setq org-priority-faces
   		'((?A . (:foreground "#ffffff" :background "#a0132f" :weight "bold"))))
  )

(use-package org-capture
  :ensure nil
  :after org
  :bind ("C-c c" . 'org-capture)
  :config
  (setq org-default-notes-file "~/Dokumente/Roam/inbox.org")
  (setq org-capture-templates
      '(("t" "Todo" entry (file+headline org-default-notes-file "Todos")
        "** TODO %?\n   %U" :empty-lines 1)
      ("s" "Scheduled Task" entry
         (file+headline org-default-notes-file "Tasks")
         "** TODO %?\n   SCHEDULED: %^t\n   %U" :empty-lines 1)
      ("n" "Note" entry
         (file+headline "~/Dokumente/Roam/notes.org" "General Notes")
         "** %?\n   %:description\n   %U" :empty-lines 1))
      )
)

;; org roam
(use-package org-roam
  :custom
  (org-roam-directory "~/Dokumente/Roam")
  (org-roam-completion-everywhere t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-M-i"    . completion-at-point))
  :config
  (org-roam-db-autosync-enable))

;; tabs

;;(setq backward-delete-char-untabify-method nil)
(global-set-key (kbd "TAB") 'self-insert-command)
(setq-default 	c-basic-offset 3
			  		tab-width 4
			  		indent-tabs-mode nil) ;;t

(fset 'yes-or-no-p 'y-or-n-p)

;; line numbers
;;(global-display-line-numbers-mode 1)
(visual-line-mode t)
(global-visual-line-mode t)

;; No sound
(setq visible-bell t)
(setq ring-bell-function 'ignore)

(setq calendar-date-style 'european
      calendar-week-start-day 1)

;;(global-set-key (kbd "C-x C-b") 'ibuffer)
(global-set-key (kbd "C-x C-b") 'buffer-menu)
(global-set-key (kbd "C-x C-k") 'kill-current-buffer)

(global-set-key (kbd "<C-wheel-up>") 'text-scale-increase)
(global-set-key (kbd "<C-wheel-down>") 'text-scale-decrease)

(global-set-key (kbd "C-c C-l") 'display-line-numbers-mode)
;; german keyboard
(global-set-key (kbd "C-M-ß") 'indent-region)

(global-set-key (kbd "<escape> <escape>") 'keyboard-escape-quit)

(global-set-key [M-up] 'move-text-up)
(global-set-key [M-down] 'move-text-down)

(defun move-text-internal (arg)
  (cond
   ((and mark-active transient-mark-mode)
    (if (> (point) (mark))
        (exchange-point-and-mark))
    (let ((column (current-column))
          (text (delete-and-extract-region (point) (mark))))
      (forward-line arg)
      (move-to-column column t)
      (set-mark (point))
      (insert text)
      (exchange-point-and-mark)
      (setq deactivate-mark nil)))
   (t
    (let ((column (current-column)))
      (beginning-of-line)
      (when (or (> arg 0) (not (bobp)))
        (forward-line)
        (when (or (< arg 0) (not (eobp)))
          (transpose-lines arg))
        (forward-line -1))
      (move-to-column column t)))))

(defun move-text-down (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines down."
  (interactive "*p")
  (move-text-internal arg))

(defun move-text-up (arg)
  "Move region (transient-mark-mode active) or current line
  arg lines up."
  (interactive "*p")
  (move-text-internal (- arg)))

(provide 'move-text)

(global-font-lock-mode 1)

(set-face-attribute 'default nil
  :font "Fira Code"
  :height 113
  :width 'normal
  :weight 'normal)

;; custom settings
(setq custom-file (concat user-emacs-directory "custom.el"))
(load custom-file 'noerror)
